package com.mw.alfanews

import com.mw.alfanews.core.news.News
import com.mw.alfanews.core.repository.converter.ConverterDbNewsToNews
import com.mw.alfanews.core.repository.db.DbNews
import org.junit.Assert
import org.junit.Test

class ConverterDbNewsToNewsTest {

    @Test
    fun convertList() {

        val converter = ConverterDbNewsToNews()

        val dbNews0 = DbNews(
            id = 0,
            title = "title0",
            link = "https://alfabank.ru/press/news/2019/9/19/56297.html?from=alfa_rss_news",
            guid = "https://alfabank.ru/press/news/2019/9/19/56297.html",
            favoritesPath = "",
            favorites = false,
            time = 1568902500000
        )

        val dbNews1 = DbNews(
            id = 1,
            title = "title1",
            link = "https://alfabank.ru/press/news/2019/9/13/56094.html?from=alfa_rss_news",
            guid = "https://alfabank.ru/press/news/2019/9/13/56095.html",
            favoritesPath = "/data/0/com.mw.alfanews/56095.html.mht",
            favorites = true,
            time = 1568370540000
        )

        val listDbNews = listOf<DbNews>(dbNews0, dbNews1)

        val listNews = converter.dbNewsToNews(listDbNews)

        val news0 = News(
            id = 0,
            title = "title0",
            link = "https://alfabank.ru/press/news/2019/9/19/56297.html?from=alfa_rss_news",
            favoritesPath = "",
            favorites = false
        )

        val news1 = News(
            id = 1,
            title = "title1",
            link = "https://alfabank.ru/press/news/2019/9/13/56094.html?from=alfa_rss_news",
            favoritesPath = "/data/0/com.mw.alfanews/56095.html.mht",
            favorites = true
        )

        Assert.assertEquals(listNews.size, 2)
        Assert.assertEquals(listNews[0], news0)
        Assert.assertEquals(listNews[1], news1)
    }
}