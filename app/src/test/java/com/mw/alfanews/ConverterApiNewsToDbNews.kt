package com.mw.alfanews

import com.mw.alfanews.core.repository.converter.ConverterApiNewsToDbNews
import com.mw.alfanews.core.repository.db.DbNews
import com.mw.alfanews.core.repository.httpclient.ApiNews
import org.junit.Assert
import org.junit.Test

class ConverterApiNewsToDbNewsTest {

    @Test
    fun convrtList() {
        val converter = ConverterApiNewsToDbNews()

        val apiNews0 = ApiNews()
        apiNews0.title = "title0"
        apiNews0.description = "description0"
        apiNews0.guid = "https://alfabank.ru/press/news/2019/9/19/56297.html"
        apiNews0.link = "https://alfabank.ru/press/news/2019/9/19/56297.html?from=alfa_rss_news"
        apiNews0.pubDate = "Thu, 19 Sep 2019 14:15:00 GMT"

        val apiNews1 = ApiNews()
        apiNews1.title = "title1"
        apiNews1.description = "description1"
        apiNews1.guid = "https://alfabank.ru/press/news/2019/9/13/56095.html"
        apiNews1.link = "https://alfabank.ru/press/news/2019/9/13/56094.html?from=alfa_rss_news"
        apiNews1.pubDate = "Fri, 13 Sep 2019 10:29:00 GMT"

        val listApiNews = listOf<ApiNews>(apiNews0, apiNews1)

        val listDbNews: List<DbNews> = converter.newsApiToNewsDb(listApiNews)

        val dbNews0 = DbNews(
            id = 0,
            title = "title0",
            link = "https://alfabank.ru/press/news/2019/9/19/56297.html?from=alfa_rss_news",
            guid = "https://alfabank.ru/press/news/2019/9/19/56297.html",
            favoritesPath = "",
            favorites = false,
            time = 1568902500000
        )

        val dbNews1 = DbNews(
            id = 0,
            title = "title1",
            link = "https://alfabank.ru/press/news/2019/9/13/56094.html?from=alfa_rss_news",
            guid = "https://alfabank.ru/press/news/2019/9/13/56095.html",
            favoritesPath = "",
            favorites = false,
            time = 1568370540000
        )

        Assert.assertEquals(listDbNews.size, 2)
        Assert.assertEquals(listDbNews[0], dbNews0)
        Assert.assertEquals(listDbNews[1], dbNews1)
    }
}