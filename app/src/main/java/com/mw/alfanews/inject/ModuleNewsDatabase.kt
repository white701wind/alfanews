package com.mw.alfanews.inject

import android.content.Context
import com.mw.alfanews.core.repository.db.NewsDatabase
import com.mw.alfanews.core.repository.db.RoomNewsDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module(includes = [ModuleContext::class])
class ModuleNewsDatabase {

    @Provides
    fun getNewsDatabase(@Named("ApplicationContext") applicationContext: Context): NewsDatabase = RoomNewsDatabase(applicationContext)
}