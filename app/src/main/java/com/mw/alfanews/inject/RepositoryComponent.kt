package com.mw.alfanews.inject

import com.mw.alfanews.core.update.FirstStartService
import com.mw.alfanews.core.update.UpdateNewsWorker
import com.mw.alfanews.ui.home.NewsViewModelFactory
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ModuleContext::class, ModuleHttpClient::class, ModuleNewsDatabase::class])
interface RepositoryComponent {
    fun inject(firstStartService: FirstStartService)
    fun inject(newsViewModelFactory: NewsViewModelFactory)
    fun inject(updateNewsWorker: UpdateNewsWorker)
}