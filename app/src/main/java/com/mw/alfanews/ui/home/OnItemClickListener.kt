package com.mw.alfanews.ui.home

import android.view.View

interface OnItemClickListener {
    fun onClickListener(view: View, position: Int)
}