package com.mw.alfanews.ui.home

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mw.alfanews.app.AlfaNewsApplication
import com.mw.alfanews.core.news.AlfaNews
import com.mw.alfanews.core.repository.Repository
import javax.inject.Inject

class NewsViewModelFactory : ViewModelProvider.Factory {

    @Inject
    lateinit var repository: Repository

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NewsViewModel::class.java)) {

            AlfaNewsApplication.get().getRepositoryComponent().inject(this)

            return NewsViewModel(AlfaNews(repository)) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}