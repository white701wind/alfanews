package com.mw.alfanews.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mw.alfanews.R
import com.mw.alfanews.core.news.News
import com.mw.alfanews.core.repository.db.DbNews

class NewsListAdapter(
    private val onItemClickListener: OnItemClickListener
):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var newsList: List<News> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_news_list, parent, false),
            onItemClickListener)
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? NewsViewHolder)?.bind(newsList[position])
    }

    class NewsViewHolder(itemView: View, private val onItemClickListener: OnItemClickListener) :
        RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                onItemClickListener.onClickListener(it, adapterPosition)
            }
        }

        private val titleNewsTextView = itemView.findViewById<TextView>(R.id.titleNewsTextView)
        private val favariteImageView = itemView.findViewById<ImageView>(R.id.favariteImageView)

        fun bind(news: News) {
            titleNewsTextView.text = news.title
            if (news.favorites) {
                favariteImageView.setImageResource(R.drawable.star)
            } else {
                favariteImageView.setImageDrawable(null)
            }
        }
    }
}