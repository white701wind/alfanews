package com.mw.alfanews.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mw.alfanews.R
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment :
    Fragment(),
    OnItemClickListener {

    private lateinit var hostNavController: NavController

    private lateinit var newsViewModel: NewsViewModel

    private var isShowFavorites = false

    private val newsListAdapter: NewsListAdapter = NewsListAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        hostNavController = Navigation.findNavController(activity!!, R.id.nav_host_fragment)

        newsViewModel =
            ViewModelProviders.of(activity!!, NewsViewModelFactory())
                .get(NewsViewModel::class.java)

        newsViewModel.getNewsListLiveData().observe(this, Observer {
            newsListAdapter.newsList = it
        })

        newsViewModel.getIsShowFavoritesLiveData().observe(this, Observer {
            isShowFavorites = it
            if (isShowFavorites) {
                switchAllOrFavorite.setImageResource(R.drawable.format_align_justify)
            } else {
                switchAllOrFavorite.setImageResource(R.drawable.playlist_star)
            }
        })

        newsViewModel.getIsRefreshingNewsLiveData().observe(this, Observer {
            swipeRefreshLayout.isRefreshing = it
        })

        newsListRecyclerView.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        newsListRecyclerView.adapter = newsListAdapter
        newsListRecyclerView.addItemDecoration(
            NewsListItemDecoration(
                5,
                context!!,
                R.drawable.divider,
                R.dimen.decorator_bottom_offset
            )
        )

        newsListAdapter.newsList = listOf()

        swipeRefreshLayout.setOnRefreshListener {
            newsViewModel.updateNewsList()
        }

        switchAllOrFavorite.setOnClickListener {
            newsViewModel.switchAllOrFavorite()
        }

        if (isShowFavorites) {
            switchAllOrFavorite.setImageResource(R.drawable.format_align_justify)
        } else {
            switchAllOrFavorite.setImageResource(R.drawable.playlist_star)
        }
    }

    override fun onClickListener(view: View, position: Int) {
        newsViewModel.selectNews(position)
        hostNavController.navigate(R.id.action_homeFragment_to_newsShowFragment)
//        hostNavController.navigate(R.id.action_homeFragment_to_sendFragment)
    }
}