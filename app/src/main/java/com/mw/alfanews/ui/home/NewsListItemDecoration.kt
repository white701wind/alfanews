package com.mw.alfanews.ui.home

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

class NewsListItemDecoration(private val space: Int, context: Context, resId: Int, resIdBottomOffset: Int) :
    RecyclerView.ItemDecoration() {
    private val bottomOffset: Int = context.resources.getDimension(resIdBottomOffset).toInt()
    private val mDivider: Drawable = ContextCompat.getDrawable(context, resId)!!

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val top = child.bottom + params.bottomMargin
            val bottom = top + mDivider.intrinsicHeight

            mDivider.setBounds(left, top, right, bottom)
            mDivider.draw(c)
        }
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        val dataSize = state.itemCount
        val position = parent.getChildAdapterPosition(view)

        outRect.left = space
        outRect.right = space

        if (position == 0) {
            outRect.top = space
        }

        if (dataSize > 0) {
            if (position == dataSize - 1) {
                outRect.bottom = bottomOffset
            }
        } else {
            outRect.bottom = space
        }
    }
}