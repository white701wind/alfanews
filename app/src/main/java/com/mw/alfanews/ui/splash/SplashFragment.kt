package com.mw.alfanews.ui.splash

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.mw.alfanews.R
import com.mw.alfanews.core.update.FirstStartService


class SplashFragment : Fragment() {

    private lateinit var hostNavController: NavController

    private val finishSplashBroadcastReceiver = object : BroadcastReceiver() {

        private val FINISH_SPLASH_ACTION = "FINISH_SPLASH_ACTION"

        val intentFilter = IntentFilter().apply {
            addAction(FINISH_SPLASH_ACTION)
        }

        override fun onReceive(context: Context?, intent: Intent?) {
            hostNavController.navigate(R.id.action_splashFragment_to_homeFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity).supportActionBar?.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_splash, container, false)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        hostNavController = Navigation.findNavController(activity!!, R.id.nav_host_fragment)

        activity?.let {
            LocalBroadcastManager.getInstance(it).registerReceiver(
                    finishSplashBroadcastReceiver,
                    finishSplashBroadcastReceiver.intentFilter
                )

            it.startService(Intent(it, FirstStartService::class.java))
        }
    }

    override fun onDetach() {
        super.onDetach()
        LocalBroadcastManager.getInstance(context!!).unregisterReceiver(finishSplashBroadcastReceiver)
    }
}