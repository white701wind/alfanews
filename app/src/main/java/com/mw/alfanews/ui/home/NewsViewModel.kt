package com.mw.alfanews.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mw.alfanews.core.news.News
import com.mw.alfanews.core.news.NewsModel
import com.mw.alfanews.core.news.ObserverStateForView

class NewsViewModel(
    private val newsModel: NewsModel
) :
    ViewModel(),
    ObserverStateForView {

    init {
        newsModel.setObserverStateForView(this)
        newsModel.setCoroutineScope(viewModelScope)
    }

    private val newsListMutableLiveData: MutableLiveData<List<News>> = MutableLiveData()
    fun getNewsListLiveData(): LiveData<List<News>> = newsListMutableLiveData

    private val isShowFavoritesMutableLiveData: MutableLiveData<Boolean> = MutableLiveData()
    fun getIsShowFavoritesLiveData(): LiveData<Boolean> = isShowFavoritesMutableLiveData

    private val isRefreshingNewsMutableLiveData: MutableLiveData<Boolean> = MutableLiveData()
    fun getIsRefreshingNewsLiveData(): LiveData<Boolean> = isRefreshingNewsMutableLiveData

    private val indexSelectNewsMutableLiveData: MutableLiveData<Int> = MutableLiveData()
    fun getIndexSelectNewsLiveData(): LiveData<Int> = indexSelectNewsMutableLiveData

    override fun newsList(newsList: List<News>) {
        newsListMutableLiveData.value = newsList
    }

    override fun isShowFavorites(state: Boolean) {
        isShowFavoritesMutableLiveData.value = state
    }

    override fun indexSelectNews(index: Int) {
        indexSelectNewsMutableLiveData.value = index
    }

    override fun isRefreshingNews(state: Boolean) {
        isRefreshingNewsMutableLiveData.value = state
    }

    fun updateNewsList(){
        newsModel.updateNewsList()
    }

    fun selectNews(indexSelectNews: Int){
        newsModel.selectNews(indexSelectNews)
    }

    fun switchAllOrFavorite(){
        newsModel.switchAllOrFavorite()
    }

    fun addNewsToFavorites(position: Int, path: String){
        newsModel.addNewsToFavorites(position, path)
    }

    override fun onCleared() {
        super.onCleared()
        newsModel.onCleared()
    }
}