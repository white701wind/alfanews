package com.mw.alfanews.ui.newsshow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager2.widget.ViewPager2
import com.mw.alfanews.R
import com.mw.alfanews.core.news.News
import com.mw.alfanews.core.repository.db.DbNews
import com.mw.alfanews.ui.home.NewsViewModel
import com.mw.alfanews.ui.home.NewsViewModelFactory
import kotlinx.android.synthetic.main.fragment_newsshow.*

class NewsShowFragment :
    Fragment(),
    OnStopScrollListener,
    OnSaveNewsListener {

    private lateinit var newsViewModel: NewsViewModel

    private var newsList: List<News> = listOf()
    private var indexSelect: Int = 0

    private val newsShowAdapter: NewsShowAdapter = NewsShowAdapter(
        this as OnStopScrollListener,
        this as OnSaveNewsListener)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_newsshow, container, false)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        newsViewModel =
            ViewModelProviders.of(activity!!, NewsViewModelFactory())
                .get(NewsViewModel::class.java)

        newsViewModel.getNewsListLiveData().observe(this, Observer {
            newsList = it
            newsShowAdapter.newsList = it
            viewPager2.setCurrentItem(indexSelect, false)
        })

        newsViewModel.getIndexSelectNewsLiveData().observe(this, Observer {
            indexSelect = it
            viewPager2.setCurrentItem(indexSelect, false)
        })

        newsShowAdapter.viewPager2 = viewPager2

        viewPager2.adapter = newsShowAdapter
        viewPager2.offscreenPageLimit = 1
        viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                newsViewModel.selectNews(position)
            }
        })
    }

    override fun stopScroll(state: Boolean) {
        viewPager2.isUserInputEnabled = state
    }

    override fun onSaveNewsListener(view: View, position: Int, path: String) {
        newsViewModel.addNewsToFavorites(position, path)
    }
}