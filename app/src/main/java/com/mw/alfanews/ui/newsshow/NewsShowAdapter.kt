package com.mw.alfanews.ui.newsshow

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mw.alfanews.R
import com.mw.alfanews.core.news.News
import com.mw.alfanews.core.repository.db.DbNews


class NewsShowAdapter(
    private val onStopScrollListener: OnStopScrollListener,
    private val onSaveNewsListener: OnSaveNewsListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var viewPager2: ViewPager2

    var newsList: List<News> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NewsPage2ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_page2, parent, false),
            onStopScrollListener,
            onSaveNewsListener
        )
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? NewsPage2ViewHolder)?.bind(newsList[position])
    }

    class NewsPage2ViewHolder(itemView: View,
                              private val onStopScrollListener: OnStopScrollListener,
                              private val onSaveNewsListener: OnSaveNewsListener
    ) :
        RecyclerView.ViewHolder(itemView) {

        private val webView = itemView.findViewById<WebView>(R.id.webView)
        private val saveWeb = itemView.findViewById<FloatingActionButton>(R.id.saveNews)

        private var news: News? = null

        init {
            webView.settings.apply {
                cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
                setSupportZoom(true)
                builtInZoomControls = true
                displayZoomControls = false
            }

            webView.setOnTouchListener { v, event ->
                if (event.action == MotionEvent.ACTION_DOWN) {
                    onStopScrollListener.stopScroll(!(event.x > 40 && event.x < v.width - 40))
                }
                false
            }

            webView.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    news?.let {
                        if (!it.favorites) {
                            saveWeb.show()
                        }
                    }
                }
            }

            saveWeb.setOnClickListener {
                webView.saveWebArchive(itemView.context.filesDir?.absolutePath, true) {
                    onSaveNewsListener.onSaveNewsListener(itemView, adapterPosition, it)
                }
            }
        }

        fun bind(news: News) {
            this.news = news
            if (news.favorites) {
                webView.loadUrl("file://${news.favoritesPath}")
            } else {
                webView.loadUrl(news.link)
            }
            saveWeb.hide()
        }
    }
}

interface OnStopScrollListener {
    fun stopScroll(state: Boolean)
}