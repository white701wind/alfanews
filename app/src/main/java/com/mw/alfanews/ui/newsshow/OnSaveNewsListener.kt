package com.mw.alfanews.ui.newsshow

import android.view.View

interface OnSaveNewsListener {
    fun onSaveNewsListener(view: View, position: Int, path: String)
}