package com.mw.alfanews.core.update

import android.app.IntentService
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.work.*
import com.mw.alfanews.app.AlfaNewsApplication
import com.mw.alfanews.core.repository.Repository
import com.mw.alfanews.core.repository.setting.ReadWriteSetting
import com.mw.alfanews.core.repository.setting.SharedPreferencesReadWriteSetting
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class FirstStartService :
    IntentService("FirstStart") {

    private lateinit var setting: ReadWriteSetting

    @Inject
    lateinit var repository: Repository

    private lateinit var constraints: Constraints

    private lateinit var updateNewsWorker: WorkRequest

    private var run = true

    override fun onCreate() {
        super.onCreate()

        val TAG_WORK_UPDATE_NEWS = "TAG_WORK_UPDATE_NEWS"

        setting = SharedPreferencesReadWriteSetting(applicationContext)

//        val repositoryComponent = DaggerRepositoryComponent.builder().moduleContext(ModuleContext(applicationContext)).build()
//        repository = Repository(
//            RetrofitHttpClient(),
//            ConverterApiNewsToDbNews(),
//            ConverterDbNewsToNews(),
//            RoomNewsDatabase(applicationContext)
//        )
//        repository = AlfaNewsApplication.get().getRepositoryComponent().getRepository()
        AlfaNewsApplication.get().getRepositoryComponent().inject(this)

        constraints = Constraints.Builder()
//            .setRequiresDeviceIdle(false)
//            .setRequiresCharging(false)
            .setRequiredNetworkType(NetworkType.CONNECTED)
//            .setRequiresBatteryNotLow(true)
//            .setRequiresStorageNotLow(true)
            .build()

        updateNewsWorker = PeriodicWorkRequest.Builder(UpdateNewsWorker::class.java, 15, TimeUnit.MINUTES, 5, TimeUnit.MINUTES)
            .addTag(TAG_WORK_UPDATE_NEWS)
            .setConstraints(constraints)
            .build()
    }

    override fun onHandleIntent(intent: Intent?) {
        intent?.let {
            if (!setting.isFirstExecute()) {

                repository.updateNewsList()

                WorkManager.getInstance(this).enqueue(updateNewsWorker)
                setting.resetFirstExecute()
            }
            Thread.sleep(1500)
        }
        finishSplash()
    }

    private fun finishSplash() {
        val FINISH_SPLASH_ACTION = "FINISH_SPLASH_ACTION"
        val intent = Intent(FINISH_SPLASH_ACTION)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
    }
}