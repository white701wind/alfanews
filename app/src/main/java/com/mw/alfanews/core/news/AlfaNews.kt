package com.mw.alfanews.core.news

import com.mw.alfanews.core.repository.OnFinishUpdateNewsList
import com.mw.alfanews.core.repository.Repository
import kotlinx.coroutines.*

class AlfaNews(
    private val repository: Repository
) :
    NewsModel,
    OnFinishUpdateNewsList {

    private var newsAllList: List<News> = listOf()
    private var newsFavoritesList: List<News> = listOf()
    private var isShowFavorites: Boolean = false
    private var indexSelectNews: Int = 0
    private var isRefreshingNews: Boolean = false

    private var observerStateForView: ObserverStateForView? = null
    private var coroutineScope: CoroutineScope = GlobalScope

    init {
        repository.onFinishUpdateNewsList = this
        repository.startUpdate()

        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                newsAllList = async { repository.getNewsList() }.await()
                newsFavoritesList = async { repository.getNewsFavoritesList() }.await()
            }

            withContext(Dispatchers.Main) {
                if (isShowFavorites) {
                    observerStateForView?.newsList(newsFavoritesList)
                } else {
                    observerStateForView?.newsList(newsAllList)
                }
            }
        }
    }

    override fun setObserverStateForView(observerStateForView: ObserverStateForView) {
        this.observerStateForView = observerStateForView
    }

    override fun setCoroutineScope(coroutineScope: CoroutineScope) {
        this.coroutineScope = coroutineScope
    }

    override fun updateNewsList() {
        isRefreshingNews = true
        observerStateForView?.isRefreshingNews(isRefreshingNews)
        repository.updateNewsList()
    }

    override fun selectNews(indexSelectNews: Int) {
        this.indexSelectNews = indexSelectNews
        observerStateForView?.indexSelectNews(this.indexSelectNews)
    }

    override fun switchAllOrFavorite() {
        isShowFavorites = !isShowFavorites
        observerStateForView?.isShowFavorites(isShowFavorites)

        if (isShowFavorites) {
            observerStateForView?.newsList(newsFavoritesList)
        } else {
            observerStateForView?.newsList(newsAllList)
        }
    }

    override fun addNewsToFavorites(position: Int, path: String) {
        val news = newsAllList[position]
        news.favorites = true
        news.favoritesPath = path

        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                repository.updateNewsFavorites(news)
                newsAllList = async { repository.getNewsList() }.await()
                newsFavoritesList = async { repository.getNewsFavoritesList() }.await()
            }

            withContext(Dispatchers.Main) {
                observerStateForView?.newsList(newsAllList)
            }
        }
    }

    override fun onCleared() {
        repository.stopUpdate()
    }

    override fun finishUpdateNewsList() {
        isRefreshingNews = false
        observerStateForView?.isRefreshingNews(isRefreshingNews)

        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                newsAllList = async { repository.getNewsList() }.await()
                newsFavoritesList = async { repository.getNewsFavoritesList() }.await()
            }

            withContext(Dispatchers.Main) {
                if (isShowFavorites) {
                    observerStateForView?.newsList(newsFavoritesList)
                } else {
                    observerStateForView?.newsList(newsAllList)
                }
            }
        }
    }
}