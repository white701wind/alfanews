package com.mw.alfanews.core.repository.httpclient

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface AlfaNewsApi {

    @GET("_rss.html")
    fun loadRSSFeed(
        @Query("subtype") subtype: Int = 1,
        @Query("category") category: Int = 2,
        @Query("city") city: Int = 21
    ): Call<RSSFeed?>


//    @GET("article.rss")
//    fun loadRSSFeed(): Call<RSSFeed?>
}