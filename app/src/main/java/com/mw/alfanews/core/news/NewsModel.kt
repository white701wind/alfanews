package com.mw.alfanews.core.news

import kotlinx.coroutines.CoroutineScope

interface NewsModel {
    fun setObserverStateForView(observerStateForView: ObserverStateForView)
    fun setCoroutineScope(coroutineScope: CoroutineScope)
    fun updateNewsList()
    fun selectNews(indexSelectNews: Int)
    fun switchAllOrFavorite()
    fun addNewsToFavorites(position: Int, path: String)
    fun onCleared()
}

