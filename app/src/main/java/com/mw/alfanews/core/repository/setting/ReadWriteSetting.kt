package com.mw.alfanews.core.repository.setting

import android.content.Context
import androidx.preference.PreferenceManager

abstract class ReadWriteSetting {

    private var _isFirstExecute: Boolean? = null

    abstract protected fun readIsFirstExecute(): Boolean
    abstract protected fun writeIsFirstExecute(state: Boolean)

    fun isFirstExecute(): Boolean {
        _isFirstExecute = _isFirstExecute ?: readIsFirstExecute()
        return _isFirstExecute as Boolean
    }

    fun resetFirstExecute() {
        _isFirstExecute = false
        writeIsFirstExecute(_isFirstExecute as Boolean)
    }

}

class SharedPreferencesReadWriteSetting(
    private val context: Context
) :
    ReadWriteSetting() {

    private val IS_FIRST_EXECUTE = "is_first_execute"

    override fun readIsFirstExecute(): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(IS_FIRST_EXECUTE, true)
    }

    override fun writeIsFirstExecute(state: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(
            IS_FIRST_EXECUTE, state
        ).apply()
    }
}
