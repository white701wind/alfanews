package com.mw.alfanews.core.repository.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [DbNews::class], version = 1)
abstract class NewsRoom : RoomDatabase() {

    abstract fun newsDao(): NewsDao
}