package com.mw.alfanews.core.repository.httpclient

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import javax.inject.Inject

abstract class HttpClient {

    var listener: ListenerQueryNews? = null

    abstract fun queryNews()
}

interface ListenerQueryNews {
    fun updateNewsApiList(apiNewsList: List<ApiNews>)
    fun errorReceivingData()
}

class RetrofitHttpClient @Inject constructor() :
    HttpClient(),
    Callback<RSSFeed?> {

    private val retrofit: Retrofit
    private val alfaNewsApi: AlfaNewsApi

    init {
        val BASE_URL = "https://alfabank.ru/_/rss/"

        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .build();

        alfaNewsApi = retrofit.create(AlfaNewsApi::class.java)
    }

    override fun queryNews() {
        val call: Call<RSSFeed?> = alfaNewsApi.loadRSSFeed()
        call.enqueue(this)
    }

    override fun onResponse(call: Call<RSSFeed?>, response: Response<RSSFeed?>) {
        if (response.isSuccessful) {
            val rssFeed: RSSFeed? = response.body()
            rssFeed?.let {
                listener?.updateNewsApiList(it.newsList)
            }
        } else {
            println(response.errorBody())
        }
    }

    override fun onFailure(call: Call<RSSFeed?>, t: Throwable) {
        println(t.message)
        listener?.errorReceivingData()
    }
}
