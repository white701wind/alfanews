package com.mw.alfanews.core.repository.converter

import com.mw.alfanews.core.news.News
import com.mw.alfanews.core.repository.db.DbNews
import javax.inject.Inject

class ConverterDbNewsToNews @Inject constructor() {

    fun dbNewsToNews(dbNewsList: List<DbNews>) : List<News> {
        val newsList: MutableList<News> = mutableListOf()
        for (dbNews in dbNewsList) {
            val news: News = convert(dbNews)
            newsList.add(news)
        }
        return newsList
    }

    private fun convert(dbNews: DbNews): News {
        val news = News(
            id = dbNews.id,
            title = dbNews.title,
            link = dbNews.link,
            favorites = dbNews.favorites,
            favoritesPath = dbNews.favoritesPath
        )
        return news
    }
}