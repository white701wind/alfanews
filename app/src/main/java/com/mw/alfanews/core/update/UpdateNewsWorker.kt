package com.mw.alfanews.core.update

import android.content.Context
import androidx.preference.PreferenceManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mw.alfanews.app.AlfaNewsApplication
import com.mw.alfanews.core.repository.Repository
import com.mw.alfanews.core.repository.converter.ConverterApiNewsToDbNews
import com.mw.alfanews.core.repository.converter.ConverterDbNewsToNews
import com.mw.alfanews.core.repository.db.RoomNewsDatabase
import com.mw.alfanews.core.repository.httpclient.RetrofitHttpClient

class UpdateNewsWorker(
    context: Context,
    workerParams: WorkerParameters
) :
    Worker(context, workerParams) {

    private val setting = PreferenceManager.getDefaultSharedPreferences(applicationContext)

    lateinit var repository: Repository

    init {
        AlfaNewsApplication.get().getRepositoryComponent().inject(this)
    }

    override fun doWork(): Result {
        return try {

            repository.updateNewsList()

            Result.success()
        } catch (error: Throwable) {
            Result.failure()
        }
    }
}
