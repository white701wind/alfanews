package com.mw.alfanews.core.repository

import android.os.Handler
import com.mw.alfanews.core.news.News
import com.mw.alfanews.core.repository.converter.ConverterApiNewsToDbNews
import com.mw.alfanews.core.repository.converter.ConverterDbNewsToNews
import com.mw.alfanews.core.repository.db.DbNews
import com.mw.alfanews.core.repository.db.NewsDatabase
import com.mw.alfanews.core.repository.httpclient.ApiNews
import com.mw.alfanews.core.repository.httpclient.HttpClient
import com.mw.alfanews.core.repository.httpclient.ListenerQueryNews
import kotlinx.coroutines.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Repository @Inject constructor(
    private val httpClient: HttpClient,
    private val converterApiNewsToDbNews: ConverterApiNewsToDbNews,
    private val converterDbNewsToNews: ConverterDbNewsToNews,
    private val database: NewsDatabase
) :
    ListenerQueryNews {

    var onFinishUpdateNewsList: OnFinishUpdateNewsList? = null

    private val updateNewsRunnable: Runnable
    private val updateNewsHandler: Handler = Handler()
    private val period: Long = 1000 * 60 * 5

    init {
        httpClient.listener = this

        updateNewsRunnable = object : Runnable {
            override fun run() {
                httpClient.queryNews()
                updateNewsHandler.postDelayed(this, period)
            }
        }
    }

    fun updateNewsList() {
        httpClient.queryNews()
    }

    override fun updateNewsApiList(apiNewsList: List<ApiNews>) {

        val dbNewsList: List<DbNews> = converterApiNewsToDbNews.newsApiToNewsDb(apiNewsList)

        GlobalScope.launch {
            coroutineScope {
                if (database.countNews() > 0) {
                    val lastTime: Long = database.lastNewsTime()
                    for (dbNews in dbNewsList) {
                        if (dbNews.time > lastTime) {
                            database.insertNews(dbNews)
                        }
                    }
                } else {
                    database.insertNewsList(dbNewsList)
                }
            }

            withContext(Dispatchers.Main) {
                onFinishUpdateNewsList?.finishUpdateNewsList()
            }
        }
    }

    override fun errorReceivingData() {
        onFinishUpdateNewsList?.finishUpdateNewsList()
    }

    suspend fun getNewsList(): List<News> {
        return converterDbNewsToNews.dbNewsToNews(database.newsList())
    }

    suspend fun getNewsFavoritesList(): List<News> {
        return converterDbNewsToNews.dbNewsToNews(database.newsFavoritesList())
    }

    suspend fun updateNewsFavorites(news: News) {
        database.updateNewsFavorites(news.id, news.favorites, news.favoritesPath)
    }

    fun startUpdate() {
        updateNewsHandler.postDelayed(updateNewsRunnable, period)
    }

    fun stopUpdate() {
        updateNewsHandler.removeCallbacks(updateNewsRunnable)
    }
}

interface OnFinishUpdateNewsList {
    fun finishUpdateNewsList()
}
