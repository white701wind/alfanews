package com.mw.alfanews.core.news

class News(
    val id: Long,
    val title: String,
    val link: String,
    var favorites: Boolean = false,
    var favoritesPath: String = ""
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as News

        if (id != other.id) return false
        if (title != other.title) return false
        if (link != other.link) return false
        if (favorites != other.favorites) return false
        if (favoritesPath != other.favoritesPath) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + link.hashCode()
        result = 31 * result + favorites.hashCode()
        result = 31 * result + favoritesPath.hashCode()
        return result
    }
}