package com.mw.alfanews.core.repository.converter

import com.mw.alfanews.core.repository.db.DbNews
import com.mw.alfanews.core.repository.httpclient.ApiNews
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class ConverterApiNewsToDbNews @Inject constructor() {

    fun newsApiToNewsDb(apiNewsList: List<ApiNews>) : List<DbNews> {
        val dbNewsList: MutableList<DbNews> = mutableListOf()
        for (apiNews in apiNewsList) {
            val dbNews: DbNews = convert(apiNews)
            dbNewsList.add(dbNews)
        }
        return dbNewsList
    }

    private fun convert(apiNews: ApiNews): DbNews {
        val time: Long = convertTime(apiNews.pubDate)
        val dbNews = DbNews(
            id = 0,
            title = apiNews.title,
            link = apiNews.link,
            guid = apiNews.guid,
            time = time,
            favorites = false,
            favoritesPath = ""
        )
        return dbNews
    }

    private fun convertTime(textDate: String): Long {
        val dateFormat = SimpleDateFormat(
            "E, dd MMM yyyy HH:mm:ss z", Locale.US
        )
        try {
            val date = dateFormat.parse(textDate) as Date
            return date.time
        } catch (e: Exception){
            return 0
        }
    }
}