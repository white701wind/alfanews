package com.mw.alfanews.core.repository.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "news" )
class DbNews(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Long,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "link") val link: String,
//    @PrimaryKey
    @ColumnInfo(name = "guid") val guid: String,
    @ColumnInfo(name = "time") val time: Long,
    @ColumnInfo(name = "favorites") var favorites: Boolean = false,
    @ColumnInfo(name = "favoritesPath") var favoritesPath: String = ""
) {
    constructor() : this(0, "", "", "", 0, false, "")

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DbNews

        if (id != other.id) return false
        if (title != other.title) return false
        if (link != other.link) return false
        if (guid != other.guid) return false
        if (time != other.time) return false
        if (favorites != other.favorites) return false
        if (favoritesPath != other.favoritesPath) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + link.hashCode()
        result = 31 * result + guid.hashCode()
        result = 31 * result + time.hashCode()
        result = 31 * result + favorites.hashCode()
        result = 31 * result + favoritesPath.hashCode()
        return result
    }


}