package com.mw.alfanews.core.repository.db

import androidx.room.*

@Dao
interface NewsDao {

    @Query("SELECT * FROM news ORDER BY time DESC")
    suspend fun dbNewsList(): List<DbNews>

    @Query("SELECT * FROM news WHERE favorites = 1  ORDER BY time DESC")
    suspend fun dbNewsFavoritesList(): List<DbNews>

    @Query("SELECT * FROM news WHERE id = :id")
    suspend fun getNewsById(id: Long): DbNews

    @Query("SELECT MAX(time) FROM news")
    suspend fun dbNewsLastTime(): Long

    @Query("SELECT COUNT(*) FROM news")
    suspend fun dbNewsCount(): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(dbNews: DbNews)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(dbNewsList: List<DbNews>)

    @Update
    suspend fun update(dbNews: DbNews)

}