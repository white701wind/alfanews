package com.mw.alfanews.core.repository.db

import android.content.Context
import androidx.room.Room
import javax.inject.Inject
import javax.inject.Named

interface NewsDatabase {

    suspend fun newsList(): List<DbNews>
    suspend fun newsFavoritesList(): List<DbNews>

    suspend fun insertNews(dbNews: DbNews)
    suspend fun insertNewsList(dbNewsList: List<DbNews>)

    suspend fun updateNewsFavorites(dbNews: DbNews)

    suspend fun updateNewsFavorites(id: Long, favorite: Boolean, favoritePath: String)

    suspend fun countNews(): Long

    suspend fun lastNewsTime(): Long
}

class RoomNewsDatabase @Inject constructor(@Named("ApplicationContext") context: Context) : NewsDatabase {

    val newsDatabase = Room.databaseBuilder(context, NewsRoom::class.java, "news.db")
        .build()

    override suspend fun newsList(): List<DbNews> {
        return newsDatabase.newsDao().dbNewsList()
    }

    override suspend fun newsFavoritesList(): List<DbNews> {
        return newsDatabase.newsDao().dbNewsFavoritesList()
    }

    override suspend fun insertNews(dbNews: DbNews) {
        newsDatabase.newsDao().insert(dbNews)
    }

    override suspend fun insertNewsList(dbNewsList: List<DbNews>) {
        newsDatabase.newsDao().insert(dbNewsList)
    }

    override suspend fun updateNewsFavorites(dbNews: DbNews) {
        newsDatabase.newsDao().update(dbNews)
    }

    override suspend fun updateNewsFavorites(id: Long, favorite: Boolean, favoritePath: String) {
        val dbNews: DbNews = newsDatabase.newsDao().getNewsById(id)
        dbNews.favorites = favorite
        dbNews.favoritesPath = favoritePath
        newsDatabase.newsDao().update(dbNews)
    }

    override suspend fun countNews(): Long {
        return newsDatabase.newsDao().dbNewsCount()
    }

    override suspend fun lastNewsTime(): Long {
        return newsDatabase.newsDao().dbNewsLastTime()
    }
}