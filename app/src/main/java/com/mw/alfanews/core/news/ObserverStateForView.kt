package com.mw.alfanews.core.news

interface ObserverStateForView {
    fun newsList(newsList: List<News>)
    fun isShowFavorites(state: Boolean)
    fun indexSelectNews(index: Int)
    fun isRefreshingNews(state: Boolean)
}