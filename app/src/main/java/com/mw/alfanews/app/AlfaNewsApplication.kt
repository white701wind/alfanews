package com.mw.alfanews.app

import android.app.Application
import com.mw.alfanews.inject.DaggerRepositoryComponent
import com.mw.alfanews.inject.ModuleContext
import com.mw.alfanews.inject.RepositoryComponent


class AlfaNewsApplication: Application() {

    private lateinit var repositoryComponent: RepositoryComponent

    override fun onCreate() {
        super.onCreate()

        instance = this

        repositoryComponent = DaggerRepositoryComponent.builder()
            .moduleContext(ModuleContext(applicationContext))
            .build()
    }

    fun getRepositoryComponent() = repositoryComponent

    companion object {
        private lateinit var instance: AlfaNewsApplication

        fun get(): AlfaNewsApplication {
            return instance
        }
    }
}